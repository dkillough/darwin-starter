#include "Species.hpp"
#include "Creature.hpp"
#include <vector>   // vector
#include <iostream>

using namespace std;
class Darwin {
	friend ostream& operator << (ostream&, const Darwin&)
	private:
		vector<Species> _species;

		vector<Creature> _creatures;

		vector<vector<Creature*>> _grid;

	public:
		// ...
};
