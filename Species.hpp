#include "Instruction.hpp"
#include <vector>   // vector
#include <utility>  // pair
#include <iostream>

using namespace std;
class Species {
	friend ostream& operator << (ostream&, const Species&);
	private:
		char                           _name;
		vector<pair<Instruction, int>> _instructions;

	public:
		// ...
};
