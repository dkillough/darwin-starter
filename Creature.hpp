#include "Species.hpp"
#include "Direction.hpp"
#include <iostream>

class Creature {
	friend ostream& operator << (ostream&, const Creature&);
	private:
		Species* _species;		// note this is a POINTER, not an array of species

		Direction _direction;

		int _program_counter;

	public:
		// ...
};
