# Kevin's Makefile edits
.DEFAULT_GOAL := all
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-11
    GTEST    := /usr/local/src/googletest-master
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
    VALGRIND :=
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/src/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
    VALGRIND := valgrind
else
    BOOST    := /usr/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-11
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
    VALGRIND := valgrind-3.17
endif

# run docker
docker:
    docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
    git config -l

# get git log
Darwin.log:
    git log > Darwin.log

# get git status
status:
    make clean
    @echo
    git branch
    git remote -v
    git status

# download files from the Darwin code repo
pull:
    make clean
    @echo
    git pull
    git status

# upload files to the Darwin code repo
push:
    make clean
    @echo
    git add .gitignore
    git add .gitlab-ci.yml
    git add Darwin.cpp
    git add Darwin.hpp
    -git add Darwin.log
    -git add html
    git add Makefile
    git add README.md
    git add RunDarwin.cpp
    git add RunDarwin.ctd
    git add TestDarwin.cpp
    git commit -m "another commit"
    git push
    git status

# compile run harness
RunDarwin: Darwin.hpp Darwin.cpp RunDarwin.cpp
    -$(CPPCHECK) Darwin.cpp
    -$(CPPCHECK) RunDarwin.cpp
    $(CXX) $(CXXFLAGS) Darwin.cpp RunDarwin.cpp -o RunDarwin

# compile test harness
TestDarwin: Darwin.hpp Darwin.cpp TestDarwin.cpp
    -$(CPPCHECK) Darwin.cpp
    -$(CPPCHECK) TestDarwin.cpp
    $(CXX) $(CXXFLAGS) Darwin.cpp TestDarwin.cpp -o TestDarwin $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    RunDarwin                            \
    TestDarwin

# compile all
all: $(FILES)

# execute test harness
test: TestDarwin
    -$(VALGRIND) ./TestDarwin
    $(GCOV) TestDarwin-Darwin.cpp | grep -B 2 "cpp.gcov"

# clone the Darwin test repo
../cs371p-darwin-tests:
    git clone https://gitlab.com/gpdowning/cs371p-darwin-tests.git ../cs371p-darwin-tests

# test files in the Darwin test repo
T_FILES := `ls ../cs371p-darwin-tests/*.in`

# check integrity of all the test files in the Darwin test repo
ctd-check: ../cs371p-darwin-tests
    -for v in $(T_FILES); do echo $(CHECKTESTDATA) RunDarwin.ctd $$v; $(CHECKTESTDATA) RunDarwin.ctd $$v; done

# generate a random input file
ctd-generate:
    -for v in {1..100}; do $(CHECKTESTDATA) -g RunDarwin.ctd >> RunDarwin.gen; done

# execute the run harness against a test file in the Darwin test repo and diff with the expected output
../cs371p-darwin-tests/%: RunDarwin
    $(CHECKTESTDATA) RunDarwin.ctd $@.in
    ./RunDarwin < $@.in > RunDarwin.tmp
    diff RunDarwin.tmp $@.ans

# execute the run harness against all of the test files in the Darwin test repo and diff with the expected output
run: ../cs371p-darwin-tests
    -for v in $(T_FILES); do make $${v/.in/}; done

# auto format the code
format:
    $(ASTYLE) Darwin.cpp
    $(ASTYLE) Darwin.hpp
    $(ASTYLE) RunDarwin.cpp
    $(ASTYLE) TestDarwin.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
    $(DOXYGEN) -g

# create html directory
html: Doxyfile
    $(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=         \
    .gitignore     \
    .gitlab-ci.yml \
    Darwin.log    \
    html

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
    rm -f *.gcda
    rm -f *.gcno
    rm -f *.gcov
    rm -f *.gen
    rm -f *.plist
    rm -f *.tmp
    rm -f RunDarwin
    rm -f TestDarwin

# remove executables, temporary files, and generated files
scrub:
    make clean
    rm -f  Darwin.log
    rm -f  Doxyfile
    rm -rf html
    rm -rf latex

# output versions of all tools
versions:
    @echo  'shell uname -p'
    @echo $(shell uname -p)

    @echo
    @echo  'shell uname -s'
    @echo $(shell uname -s)

    @echo
    @echo "% which $(ASTYLE)"
    @which $(ASTYLE)
    @echo
    @echo "% $(ASTYLE) --version"
    @$(ASTYLE) --version

    @echo
    @echo "% which $(CHECKTESTDATA)"
    @which $(CHECKTESTDATA)
    @echo
    @echo "% $(CHECKTESTDATA) --version"
    @$(CHECKTESTDATA) --version

    @echo
    @echo "% which cmake"
    @which cmake
    @echo
    @echo "% cmake --version"
    @cmake --version

    @echo
    @echo "% which $(CPPCHECK)"
    @which $(CPPCHECK)
    @echo
    @echo "% $(CPPCHECK) --version"
    @$(CPPCHECK) --version

    @echo
    @echo "% which $(DOXYGEN)"
    @which $(DOXYGEN)
    @echo
    @echo "% $(DOXYGEN) --version"
    @$(DOXYGEN) --version

    @echo
    @echo "% which $(CXX)"
    @which $(CXX)
    @echo
    @echo "% $(CXX) --version"
    @$(CXX) --version

    @echo
    @echo "% which $(GCOV)"
    @which $(GCOV)
    @echo
    @echo "% $(GCOV) --version"
    @$(GCOV) --version

    @echo
    @echo "% which git"
    @which git
    @echo
    @echo "% git --version"
    @git --version

    @echo
    @echo "% which make"
    @which make
    @echo
    @echo "% make --version"
    @make --version

ifneq ($(shell uname -s), Darwin)
    @echo
    @echo "% which $(VALGRIND)"
    @which $(VALGRIND)
    @echo
    @echo "% $(VALGRIND) --version"
    @$(VALGRIND) --version
endif

    @echo "% which vim"
    @which vim
    @echo
    @echo "% vim --version"
    @vim --version

    @echo
    @echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
    @grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

    @echo
    @echo "pkg-config --modversion gtest"
    @pkg-config --modversion gtest
    @echo
    @echo "% ls -al $(LIB)/libgtest*.a"
    @ls -al $(LIB)/libgtest*.a
